core = 7.x
api = 2

; uw_conditional_rulesets
projects[uw_conditional_rulesets][type] = "module"
projects[uw_conditional_rulesets][download][type] = "git"
projects[uw_conditional_rulesets][download][url] = "https://git.uwaterloo.ca/wcms/uw_conditional_rulesets.git"
projects[uw_conditional_rulesets][download][tag] = "7.x-2.1"
projects[uw_conditional_rulesets][subdir] = ""

; uw_co-op_skills_prioritization_tool_template 
projects[uw_coop_skills_prioritization_tool_template][type] = "module"
projects[uw_coop_skills_prioritization_tool_template][download][type] = "git"
projects[uw_coop_skills_prioritization_tool_template][download][url] = "https://git.uwaterloo.ca/wcms/uw_co-op_skills_prioritization_tool_template.git"
projects[uw_coop_skills_prioritization_tool_template][download][tag] = "7.x-1.6"
projects[uw_coop_skills_prioritization_tool_template][subdir] = ""